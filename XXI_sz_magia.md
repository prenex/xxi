% Aranymagor – a fehér ló útja

<span id="anchor"></span>Előszó
===============================

Abban reménykedem, hogy nem derül ki a nevem. Nem félek, hogy a mag tán
gyümölcsöt se terem sőt bízva-bízok, hogy célba talál a fegyverem. Azért
bízok, mert az íjat nem magamért feszítem. Istent magát kérem: ne legyen
hírnevem! A győzelemre esküszök: nem magamért készítem.

	Mi az a mágia? Milyen út vagy vágta?
	Mennynek és földnek elveszett koronája,
	Istennek dicsősége, szentek áldomása,
	Szerelem tüzének dicső ragyogása,
	Győzelem nyilának megvillanása,
	Egyenesen annak szívbehatolása,
	ott soha nem pusztításra,
	hanem gyógyításra.

Sokan talán csodát keresnek majd ebben a könyvben, vagy varázslatot –
ígéreteket, esetleg nacionalista felhangokat. A varázslat azonban nem
mágia és a mágia nem varázslat! Amit olvashatunk nem "titkos" tudás és 
főként nem titkolni való – hanem egy maréknyi arany mustármag.

	Nem rejtegették, hanem eltemették,
	Mindig is őrízték, mindig is szerették,
	Világnak porai már régen befedték,
	Arany szegeletét oly sokszor felégeték,
	Soha meg nem törték, szunnyadó életét.

Amit feltárunk az nem egy dicső, bár letűnt múlt, hanem parázsként élő
és a nyelv, a kultúra és igazosság által elérhető élet. Olyan út – a tű
fokán keresztül – ahol csak nyílhegyek hatolnak át. Nem a vér, hanem a
megismerés, az önmagunkba, belső „mag”-unkba való befogadás örökölteti
át.

	Nevünk megőrízte,
	Nyelvünk éltette,
	Mindenkiért tette,
	Mindenki érthette.

– a legfontosabb tanítása pedig abban áll, hogy másoknak meg tudjuk
mutatni, hogy van népi értéken alapuló alternatívája annak, hogy a
sokszínűséget elvesztve olvadjon össze az emberiség Bábel tornyának új
oltárán! Nem adunk majd elő rituálékat különböző természetvallásokból,
hanem magát az életet fogjuk megszólítani – egészen közvetlenül. Olyan
dolgokról, amelyek külsőségekről szólnak, mi itt nem fogunk sok szót
ejteni. Amit ebben a könyvben olvashatunk az nem népszerű és nem is
egyszerű, de mégsem tudós közösségnek szánjuk, hanem hősöknek. Olyan
hősöknek adjuk akikben a lélek fényesen világít, a mag pedig majd a
szívben ragyogva várja, hogy igazi magosságokba törjön! Ők azok a
nemzetünkben és mindenhol a földön akik életet tartanak. Ők azok akik
miatt a „mágia” valósággá válik és csakis bennük bízunk.

Lovat, íjat és nyilakat adunk, azért hogy győzzenek – a magos hegy
szélétől, a csillagokig is lőjenek – akár szolgának, akár gazdag
Rotschildnak szülessenek!

Nem fogunk itt tehát kitérni hosszas filozófiai viták bizonyítás jellegű
eldöntésére, illetve olyan győzködésre, amely csak arra szolgál, hogy a
teljesen megtévedett embernek felnyissuk a szemét. Fontos, hogy nem
próbáljuk megerőszakolni környezetünket, de tudatosan használható, okos
fegyver gyanánt néhány gondolatot át kell adunk. Ennek ellenére a cél
inkább azon képességek fejlesztése, amivel mi magunk válhatunk saját
eszünk és szívünk fegyvereinek kovácsaivá.

Ez a könyv azokhoz szól, akik a nyitottak a magok befogadására és
azok kinyílására várnak. Azoknak kik az igaz élethez kérnek hatékony
eszközkészletet, de nem a múlt sérelmei, a felgyülemlett sérülések
szerint látják a dolgokat, hanem még eredeti tisztaságukban. Azokhoz
szól a könyv, akik növekedésre áhítoznak, hogy aztán égig érő faként
felnőve, ezernyi új maggal termékenyítség meg az emberiséget azokkal a
magvas gondolatokkal, ami a jó értékek felemelkedését szolgálja. Az
egészséges rügyet, ki nem nyílt virágot nem permetezik le és szórják
körbe kénporral, ezért az olvasóval szemben alkalmazott bánásmód is
különbözik majd attól az érvelési módtól, amire ellenséges lelki
környezetben kell szorítkoznunk sok esetben. A magot megszaporítjuk és
elvetjük. A csipegető madarakkal nem foglalkozunk.

A mag bennünk van és a tisztaság, az igaz emberség megismerésével
felövezve magunkat az igaz emberként való életre majd azokon is
segíthetünk tán, akik az itt leírtakat csak sokkal több, hosszabb,
összetettebb magyarázat mellett értenék meg. Ha a nemzetben csak
ötvenezer igazi mágus lenne, akkor ma is legendák születnének és nem
kellene arra várni, hogy mikor lesz a Magyar név, megint szép! Ehhez
mérten továbbá, ha a világban csak ötmillió igaz mágus van, akkor
reménykedhetünk, hogy a sátán még nem győzedelmeskedett, de majd látni
fogjuk, hogy bámikor akár egyetlen igaz ember létével is még menthetőnek
nevezhetnénk a helyzetet ahogyan azt Isten kinyilatkoztatásaiból
ismerjük. Nem mindenható pártokban, hanem a saját lelkünk, nemzetünk
felemelkedésében, alulról építkezve válthatjuk meg a hazánkat és a
világot!

Pont elég olyan könyvet olvashatunk, ami könnyű életet ígér, vagy a
fantáziánkkal játszik, így azt javaslom örüljünk, hogy ez az írás nem
ilyen! Aki megemészti és továbbgondolja azt a bölcsességet, ami itt
megosztásra kerül nem könnyű és nem is varázslatokkal teli életre
vállalkozik, hanem megerőltető hőstettekre és mágiára. A mágia –
csakúgy, mint a Magyar népnév, Magor nyomán – a magból származik. Milyen
egy igazi Arany-Magyar, magos, mágus? Nem sámán és nem is varázsló,
hanem olyan ember, aki veszélyesebb holtában, mint élve! Azért veszélyes
holtában, mert az elpusztításához csak a lelkének a felemése árán lehet
eljutni! Egy igazi mágussá válva olyan magosságos legyen az, amit ti
cselekedtek, hogy ha el is vesztek, vagy népünk el is vész, tetteink és
világnézetünk még akkor is feltámadhasson, ha csupán egyetlen apró
elfeledett és eltemetett magva marad meg a föld alatt arra várva, hogy
valaki tiszta lélekkel egyszer sok-sok ezer év múlva rátaláljon és azt
magáévá, tehát saját magjává tegye!

Aki Magyar az ebbe a kultúrkörbe született és az a feladatunk, hogy ezt
soha ne hagyjuk veszni. Az olvasó nem könnyű életet, hanem igazi harcot,
célt szerez magának, amelyért lélekben meghalnánk. Olyan célt, amit
akkor is elérünk, ha nagyon nehéznek, sőt kilátástalannak tűnik az
erőfeszítés. Olyan jövőt, melyet nem csak gyermekeinkre, hanem az
emberiségre hagyományozunk. Olyan világba születtetünk, ahol a legtöbb
nemzetek elvesztették már mindezt, például torzult nacionalizmus ártó
árnyékában, vagy épp ellenében – de ezt nekünk még akkor is meg kell
őrízni, ha ebbe pusztulnánk.

Ez az út egy tanítás útja: annak a tanítása a világ népei felé, hogy
becsüljék meg önmagukat, múltjukat és nemzetüket és mindenki mást – ha
különböznek is – erre tanítsanak tovább!

Nehéz lesz-e? Az nem kifejezés! Olyan vállalkozás ez, amely látszólag
lehetetlen, amelyre nem számítanak sehol, amely a világ ellenében,
az élet szerelmében, de hősi diadal jegyében ír - sőt ró - történelmet!

Mégis szép csendben rója a történelmet, rejtett módon, zaj nélkül.

<span id="anchor-1"></span>Ami tudvalevő Aranymagorról
======================================================

<span id="anchor-2"></span>Aranymagor dala
------------------------------------------

	Fehér lovon vágtál,
	Koronája örök,
	Íja célba talál,
	Ég is beledörög.

	Király vajon ő tán?
	Úr a népek között?
	De hisz nép maga is!
	Az utolsók mögött!

	Népekért száll lóra,
	Másokért könyörög!
	Nem válhat valóra:
	Bábel? – nem, köszönöd.

	A jó Istentől várja,
	Hogy reád tekintsen,
	Hozzá szól imája:
	„Másokon segítsen!”

	Szín-aranyban úszik,
	De pénze sok nincsen.
	Lelkében mély hit
	Szegeletén kincsem.

	Vérét nem sajnálja,
	Teste: igaz nincsen.
	Tagjai mi vagyunk,
	Aranymagyar szívben.

<span id="anchor-3"></span>Hősi küldetése
-----------------------------------------

Kérdezték is az emberek, bölcsek és gyermekek: Kicsoda Aranymagor? Egy
személy? Egy vallás? Egy eszme, vagy egy hitvallás? A táltos és a
garabonciás szerint egyik sem, de nem mert mindezeknél kevesebb, hanem
mert mindezeknél több, akár a látomás: Aranymagor Bábel kulcsának utolsó
őre, az emberi civilizáció szakértője, főnix népe, fehér ló népe, magvas
gondolatok képe – és mindezek megszemélyesítése. Mágikus mesehős, új
korok szárnya – gyönyörű mezőknek példás unokája. Táltos lován a világot
bejárja, csodát hoz létre, sok szépet a világra. Akkor élt, amikor már
csak az igazi mágia segíthetett, mert a világ és mindenség életében újra
nagy zavar kerekedett. Nagyhalak korában, halál fenyegetett – és az
emberiség a félelmében a sok kiömlő víztől – újra tornyot építgetett.

Olyan korok jártak, amikor még néhányan tudták, hogy vízzel álmodni
rosszat jelent, de már rég azt sem értették miért vízözön képében írták
meg az egyik előző, hasonló történetet – bolond idők voltak ezek!
Aranymagor nem értette, vajh ilyen ostobaság hogyan lehet – míly dolog
nem látni azt, ami olyan érthetően ott lebeg? De hát nem látták azt sem,
hogy nincs különbség! Nincs aközött ki másokat pusztít és ki mindenkit
egyszerre pusztít épp! Oszd meg és uralkodj – ez volt a beszéd – s oly
sokan hallgatták, hogy egyénnek lenni lassan minden nép-ismerőse félt:
„Ha mindenki leszek, nem leszek megosztott és ha mind meghalunk, az
embereket pedig magukra hagyjuk – majd csak találnak maguknak új
népeket: gyárakat, szektákat, sőt elmebetegségeket! Majd mindezek új
néppé lesznek.” Ezeket mondták az ostobák, de Magor fia Aranymagor rájuk
nem hallgatott, mert épp apukája arany szegelet kertjében, a csodás
szarvaskájának, szép kis házikót rakott.

Nép s nemzet volt ő is, akárcsak vén nomád atyja és hitt Isten szavának
mint annak boldog asszonya, az ő anyja. Ismerte még régről Bábel
tornyának történetét, értette a nyelvzavar okát, őrízte régi állapotát.
Vajh nem tanul senki más, ki mind ott volt apjával, vagy vele? Néha
már-már tényleg könnybe lábad szeme. Hát nem látja a sok barát, ellenség
és testvér? Nem látnak szemüktől milyen nagy a veszély? Amikor előtör a
gonosz, tolvaj világ-birodalom... a semmiből a minden összezavarodik és
kezdődhet elölről. Hát nem tudják, hogy minket népeket arra teremtettek,
hogy több- és többféleképp álljunk a gonosszal szemben és ne találjon az
ördög minden embert egyszerre fogó fegyverre?

Szegény Aranymagor így tanakodott, majd apja egyszer csak ajtaján
bekopogott. Vén Magor ugyanis ezt nejével is tisztán látta – éppen ezért
fogott bele anno egy új, tiltott mutatványba: Gyermeket nemzett hát
saját kertjén belül, akit boldogságtól sugárzó anyjával Aranymagor-nak
neveztek és hamarost hüvejk-piciny bölcsőbe helyeztek. Itt élt a pici
kertben és csendben nevelkedett – egészen addig, míg felcseperedett.

Kihívja az apja – tyűha, mi lőn vajon? Sok mindent gondol szegény, ilyen
fiatalon…

- Üljünk le mostan fiam és beszéljünk anyáddal, lehívjuk az égből, de
repüljön madárral!

És repült túlról madár, repült egykettőre – máris bensőséges, hangulat
lett tőle.

- Gyere fiam, gyere s nézz jó mélyen magadba: látod-e Székely szívem ott
lenn a mellkasodba? Látod-e Magyar hőseim a bátor kezedben és anyukád
szentjeit gyönyörű szemedben? Meg kell ejtenünk most az apa-fia
beszédet: lassan eszes leszel és magadat felméred! Gondoltál-e arra,
vajon mily különböző vagy? Hogy lehet hogy szüleiddel kicsit-kicsit egy
vagy? Nézz körül a kertben, haldoklunk mink, népek! A sötét lúdvérc
sárkány mindent megigézett!

	Körbenéz a legény:
	szeme tágan járja,
	körül a vidéket,
	sok szomorút látva.

	Egyfelől a sárkány,
	uszítja a népekt,
	másfelől készíti,
	az olvasztó tégelyt.

- Apám, vak mindenki? Hová lesz a drága, csillagos egeknek sok szép
kultúrája?

- Nem vész még el fiam, volt már ilyen Róma... hanem nyisd ki füled
tágra és most vigyázz a világra: Téged azért neveltünk, hogy legyen
olyan nép, akire mind ki a múltját keresi büszkén néz! Aki reményt,
hitet és szeretetet visz zászlóján, amikor nagy a vész és táltos
paripáján olyant dobbant, hogy arra minden szív megdobban! Nem
vérből, hanem szellemből való vagy te, mint valaha én, apám és 
mind a szüleink. Olyan népek, melyek nem hellyel, nem származással,
hanem helyes szívvel, tettekkel és a közös belső világ elfogadásával
születtek meg szerelemből - emberekből akik nem csak élnek, de 
szeretnek, remélnek és egymásért örömmel tennének. Olyan, világ ez
ahol a emberekre találsz ki Rómával szemben beáll Atilla seregébe
és bárki legyen is, máris Szittya - ha már Szittya, viselete, dala,
beszéde és szíve! Ez az mi el lett ásva fiam jó mélyre, hogy mi alkot
egy népet és hogy született a közös ének! Addig szép az élet, amíg
nem egyedül éled, de egymásnak lehet ugrasztani a fiatal nemzedéket,
majd odanyújtani a mézes cukros süteményed: újra birodalmat építeni,
egy égig éri tornyot ahol minden egyfélén fortyog. Ez a sárkány játéka
aki a háttérben kavarog. Minden 12 hónapban elpróbálja ez a barom és
mindig volt aki kibírta állva - hát most jól figyelj a tanulságra.

	Azzal pedig apja,
	megkezdé meséjét,
	Boldog anyukája,
	segíté beszédét.

	Repül túlról madár,
	suhog mélyen szárnya,
	Hogy a földig leér,
	Rezgő csapkodása.

	Leszáll vele szava
	Atyának, Anyának:
	Fel a táltos lóra!
	Induljunk a tájnak!

	Ha van füled hallod,
	Ha szemed hát látod,
	Vágtassál mivélünk,
	Rá termettél, látod!

	Halld az öreg szavát,
	Add hozzá szívedet,
	Tisztasággal kérlek,
	Teremts újra minket!

<span id="anchor-4"></span>Ami tudvalevő a mindenségről
=======================================================

A mágus, azaz magos az, aki magosságot cselekszik, „mert a világon élni,
csak hősként érdemes”! Reszkess babilon, mert bár a téglák erősek, a
falak vének, de az alap – a talapzat – hibája a régi. Eljárt az idő azok
felett akik nem teszik fel a fontos kérdéseket és tovább építik a
szörnyeteget! A mindenség földje lesz ami megremeg: nem a világi téglák
és elvek azok, amik elhozzák végzeted...

<span id="anchor-5"></span>A tisztán látás jellemzői
----------------------------------------------------

	Aki vak az nem lát csak néz,
	Az egó a szép szemed lakatja.
	Igaz légy, mielőtt kihuny a fény!
	A halál mindenképp felszaggatja!

Amikor tisztán látsz és ezt meggondolod, akkor majd életed adósságát
siratod. Amint magadat feladod, amint magad választását az éghez szabod
– remélve, hogy így legalább egy részét talán megkapod – akkor veted el
csak igaz emberként a magot. Addig minden nap van mit takarítanod.

<span id="anchor-6"></span>Az univerzum és a világ
--------------------------------------------------

	A fényeset keresed, vagy a mindenséget?
	Ha kevered elnyerted a hülyeséget!

Valamikor mindenek tudák, még a pásztor legények is, hogy a mindenség
az minden, abban ott van Isten: abban ott a legény és aranyos szellem.
A nyelv nem hazudik: a minden az minden. Hogy ha máshogy lenne, más név 
lenne itten. A világ az más dolog, fényes és világos: hogy ennek azt
nevezzük, ami látható a virág most. Ha máshonnan nézed, láthatsz más
virágot, de ez csupán az érzet: egy kertben van, látod?

	Ha több mindenséget lát,
	részeg hát a lelked!
	Sok világban járhatsz,
	de mindenből? Csak egy lesz!

Tudták ezt még később, más népek s családok, nem csak piciny arany
szegeletben látod: Univerzum - mondták - és értették a számot!

Mondd, írd, hogy az univerzum: mindenség – egység. Nincs több és trükközés!
Nincs az univerzumon kívül semmi, mindent (még Istent is) bele kell érteni!

Világ: Ez amit tapasztalattal, törvényekkel leírunk és amiben élünk.
Akár egy bolygóra, akár a csillagokra nézünk. Ha túl okosak vagyunk
sok egymás mellettit remélünk és mint sötét erdők: elfedik mit nézünk.

	Első az Isten s a Mindenség,
	benne Nemzet, Család s Ember,
	Hogy mit miért mutat fényesség,
	Hasznos tudnod, de nem kell!

<span id="anchor-7"></span>A kilőtt nyíl előrehaladása
------------------------------------------------------

Ha kilövünk egy nyilat légüres térben mi mozgatja előre? Egyenes vonalú
egyenletes mozgásban van ezt tudjuk tanulmányaink alapján, na de vajon
mi az a kiváltó ok, ami miatt minden időpillanatban változik a
tulajdonsága (helyzet attribútum)? Ennek ugyanis változnia kell a
fizikai modelljeink szerint, ám miért változik, ha semmi ok nem hat rá?
Varázslatban hiszünk hát?

Ismerjük fel: a modell elnagyolt, helytelen. A valósághoz közelebb áll
az a gondolkozásmód, hogy a nyilat kilőttük és ezzel cselekedtünk, okot
adtunk, a mindenséget pedig így ezzel olyan helyzetbe, vagy állapotba
hoztuk, mintha ezzel fel lett volna írva egy papírra, hogy mikor és hol
indult el az a bizonyos nyílvessző. Ha ezek után valaki meg szeretné
figyelni a nyílvesszőt, mint egy személy, akkor az elmegy, elolvassa a
papírt és értelmezi azt. Megnézi a karóráján mennyi idő telt el és ő
megteremt magának egy látványt, amiben a nyílvessző a megfelelő ponton
helyezkedik el. Ő ezért érzékeli ott, ahol van. Nincs varázslat, ez
mágia – vagyis a dolgok igazi magva. Ha ezt most értjük, akkor azt is
megértjük majd, hogy a változás látszólagos.

<span id="anchor-8"></span>A dolgok lényege
-------------------------------------------

I, II, III, IV; 1, 2, 3, 4; a, b, c, d. Látjuk-e mindezekben ugyanazt az
egyetlen létezőt több megjelenési formában, manifesztációban? Látjuk-e,
hogy ilyen módon tud az Isten pl. emberként a világban Jézusként
megjelenni? Ez ilyen egyszerű. Nem a megjelenési mód, hanem az egymásnak
egyértelműen megfeleltethető lényeg az ami számít. Ha egy ember –
teljesen hús vér ember, aki pontosan ugyan olyan, mint te és én –
pontosan úgy viselkedik és pontosan azokkal a jellemzőkkel bír, amivel
Isten lényege, akkor ő maga tényleg a földre szállt Isten, benne
megtaláljuk Isten lényegét. Ha a szent lélekről való tanításokat nézzük,
ott azt látjuk, hogy Istennek egy más megjelenési módjáról beszélünk, de
megint csak ugyan arról az egyetlen létezőről gondolkozunk.

TODO: Síkbeli világ és egydimenziós változat – minden valamire való és a
magasabb matematikához értő ember képes adni leképezéseket több
dimenzióból, eggyel kevesebb dimenzióba. Akkor miről beszélünk a világ
dimenzióit tekintve? Miért beszélünk félre? Miért gondolkozunk ennyire
emberien?

TODO: Mi azonos és mi különböző, mi az hogy valamink a lényege?

TODO: nem csak Istenes példák

<span id="anchor-9"></span>Valószínűség
---------------------------------------

Aki túlzottan bízik a valószínűégszámításban és statisztikában annak a
következő gondolatkísérletet ajánlom: Fogjunk két pénzérmét a két
kezünkben – a pénzek legyenek teljesen azonosak. Ha egyszerre rázni
kezdjük a kezeinkben a pénzt és egyszerre felmutatjuk, akkor mekkora
valószínűséggel kapunk pont két fejet?

Egyszerű matematika. A számításhoz szükség van a kedvező esetek számára
és az összes eset számára. A kedvező esetek száma 1 db – hiszen a két
pénzdarab csak egyféleképpen lehet egyszerre fej. Mennyi az összes eset
száma? Erre itt most két variációt is adunk:

Érdemes elgondolkozni ezen, persze aki empirikusan kipróbálja, az majd
rájön akkor is, hogy melyiket kell használni, ha esetleg nem tudná már
most a választ, hogy ¼, vagy 1/3 a valószínűség. Az elgondolkodtató
dolog az egészben csupán annyi, hogy mind a kettő modellnek van
látszólag létjogosultsága, mind a kettő lehetne az, amely a valóságot
leírja. Ha majd mélyebben belegondol az olvasó, akkor esetleg eljöhet
olyan idő, amíg egy hosszabb-rövidebb gondolatmenet idejéig úgy véljük,
hogy tudjuk miért is kell pont azt választani amit az empirikus
eredmények alapján helyesnek látunk, de ez csak felületes
elbizakodottság: tényleg mind a két modell helyes lehetne, hiszen
tényleg ugyanolyan pénzekről van szó, tényleg módunkban sem áll
megkülönböztetni az egyiket a másiktól – de ha nem figyeljük meg őket
akkor talán ez azt is jelenti, hogy akkor éppen tényleg más esélyeket
kapunk. Nem tudjuk igazából ellenőrízni, hogy a megfigyelés elhagyásával
megváltozik-e a viselkedés, mert erre elméleti lehetőség sincsen.

Nem azért történnek a dolgok a valószínűségszámítással összhangban, mert
a matematika egyetemes, hanem a matematika alakult ki eredetileg
empirikus megfigyelések segítségével vizsgált rendszerek hatékony
támogatására. Az, hogy némely eleme, mint pl. az absztrakt algebra nagy
hasonlóságot mutat metafizikai kérdésekkel, más elemei pedig mondjuk
inkább praktikus ismereteket hordoznak, leginkább történelmi vonása
ennek a rendszernek – nem pedig valami mélyebb igazságot tár fel.
Sajnálatos, hogy úgy alakítjuk kutatásainkat, hogy az empirikus
megfigyeléseink nélkül semmire se mehetünk, hiszen az empirikus
tudományos módszer hiten alapuló, bizonytalan eljárás mint azt látni
fogjuk. Mégis milyen sokan vannak, akik azt sem tudják, hogy milyen
dolgokban hisznek, hanem vakon hisznek abban, hogy a múltbeli
ismereteink és az empirikus eredményeink helyesek!

<span id="anchor-10"></span>A múlt bizonytalansága
--------------------------------------------------

TODO: A hitről és a bizonyíthatóság látszatáról… Ez tudományos szemmel
nyilvánvaló, de úgy kellene leírni, hogy mindenki megértse. Példákkal,
képekkel és hasonlattal talán...

Olyan ember nincs, aki nem hisz. Olyan ember sajnos van, aki azt
gondolja, hogy ő hit nélkül él. A tudományos világi eljárások nem
hordoznak bizonyosságot, csak olyan ürességet, ami bizonyosságnak tűnik.
Gondolkozzunk el a következőn: Isten megteremti a világot, de nem
Ádámmal és Évával, hanem úgy, hogy minden egyes kis része az 5
másodperccel ez előtti állapotban keletkezik meg. Hat másodperccel
korábban tehát nem létezne a világból semmi, 5 másodperccel ezelőtt
viszont a fejeinkban élethossznyi emlékekkel teremt meg minket a
teremtő. Ez persze direkt egy olyan gondolatkísérlet, ami sarkított –
nehogy elhiggyük, hogy ez egy komoly állítás!

Mi azonban a dolog hozadéka? Ha a világ csak öt másodperce keletkezett –
pontosabban, ha annak előtte az Istenen túl semmi más nem volt része az
univerzumnak és mi nekünk évezredes írott történelmünk és évtizedes
emlékeink vannak erről a nem létező periódusról, akkor ilyen helyzetben
mit tekinthetünk valódi információnak emlékeink közül? Nem-e hamis
légvár ilyen elképzelt feltételek között, hogy ha elejtem a labdát,
akkor tudom: az le fog esni?

Mondhatjuk: nem, hiszen le tudjuk ellenőrízni és tényleg le is esik –
ezzel pedig megtudtuk, hogy a feltételezéseink helyesek. Azonban nem-e
lehet ezt a gondolatmenetet eljátszani akárhányszor? Milyen tudásra
teszünk szert azáltal, hogy 10 másodperce leellenőríztük, hogy nem 15
másodperce keletkezett a világ? Hiszen a fenti sarkított
eszmefuttatásban nem tudhatjuk mikor keletkezett! Ha visszakövetjük az
eseményeket az ősrobbanásig, mi csak számításokat követünk végig, nem
feltétlenül a valóságot – ebből a példából látszik nagyon jól, hogy egy
olyan feltételezéssel élünk minden egyes tudományos levezetés vagy akár
kísérleti eredményen alapú érvelés esetén, ami egyáltalán nem biztos:
Hogy a múlt nem bizonytalan, hogy egyáltalán van esélyünk bízni legalább
részleteiben annak, amit korábbi kísérleteink alapján tudottnak
vélünk...

Ha elképzeljük a fenti helyzetet, akkor nyilvánvalóvá válik, hogy
akármennyi kísérletet hajtunk végre, akárhányszor ismételjük meg a
meglévő tudásunk ellenőrzését, soha nem lehetünk biztosak abban, hogy
ami a fejünkben emlékként szerepel az valóságos létező. Majd látni
fogjuk hogy ha azonban univerzumként próbáljuk vizsgálni a közeget
amiben élünk, akkor ha nem is biztos válaszokat, de megnyugtató
lehetőségeket kaphatunk. Az empirikus módszer azonban az univerzum
megismerésére nem alkalmzható ilyen biztonsággal, sokkal alkalmasabb
megfelelő irányú hit mellett a praktikus ismeretek bővítésére és a világ
jobbá tételéhez. Ahhoz, hogy a mindenséget ismerjük, már többre van
szükség. Az empirikus módszereink sajnos „vakon” feltételezik a múlt
megbízhatóságát és hogy „korábbi” eseményekről megbízható információink
állnak rendelkezésre.

Mondhatjuk: Ha nem is empirikusan, de elméleti oldalról be lehet
mutatni, hogy ez az okoskodás felesleges. Matematikailag ugyanis kicsi
az esélye annak, hogy pont akkor és pont ott keletkezett az univerzum.
Jajj ekkor azonban megfeledkeztünk arról, hogy vajon milyen matematikai
elvek alapján dolgozunk ebben az eszmefuttatásban?! Nem olyan elvek
alapján, melyeket azért tartunk célravezetőnek, mert azt képzeljünk,
hogy jól modellezik a valóságot a fejünkben lévő tudás alapján? Ha ebben
a rossz materiális gondolkozásmódban tobzódunk, akkor vegyük észre, hogy
egy teremtő – amennyiben rosszindulató lenne – megtehetné, hogy minden
pillanatban máshogyan alkot meg minket és olyan emlékekkel, ami
célzottan nem megfelelő szabályrendszert mutat nekünk. Ilyen világban
újra és újra csak elbuknánk – minden egyes előzetes elképzelésünk
kudarcot vallana, majd miután elengedjük a labdát és az mégsem esik a
földre, egy olyan más világban találnánk magunkat, ahol mondjuk felfelé
repülő labdát várunk és ott egy ilyen kísérlettel az leesik a földre… Ez
egy elég gonosz kis gondolatkísérlet, de elméleti síkon ettől még nagyon
is elképzelhető. Gyakorlatilag ez mit is jelent? Azt, hogy ha ilyen
elképzelhető, azzal egy dologban lehetünk biztosak: hogy semmiben sem
lehetünk biztosak! Hiszen amennyiben pont ez az eset állna fent, akkor a
valószínűségszámítás, statisztika, matematika (vagy akár a beszélt
nyelvről alkotott elképzeléseink) tehát a rendelkezésünkre álló korábbi
modellek és fizikai számítások mind használhatatlanok lennének, így a
velük való érvelés vagyis az, hogy elméletileg minek kicsi az esélye nem
számítana semmit sem. Mindezek mellett, ha ez valakit megnyugtat:
tetszőlegesen nagy számosságú olyan eshetőség létezik, ahol ez a
bizonytalanság fennáll. Ha tehát mindenképpen arányítani szeretnénk,
akkor vegyük észre azért azt a problémát is, hogy ez még akkor se lenne
túl szerencsés helyzet, ha egyébként nem tenne maga az elgondolás minden
számítást feleslegessé.

*További gondolat 1:* Ez a gondolatkísérlet falszifikálhatatlan.
Korábban többen úgy vélték, hogy nem falszifikálható dolgokkal nem
érdemes foglalkozni. A fenti példa helyes értelmezésével kiderül, hogy
nagyon is megeshet, hogy egy nem falszifikálható gondolatkísérlet,
elképzelés lehetősége olyan következményekkel jár, amely aláássa a
tudományos módszereinket amennyiben fennállna. Ilyen helyzetben ha nem
tudjuk kizárni, hogy ez a helyzet áll fenn, akkor olyan helyzetben
vagyunk, hogy „találtunk egy lehetséges esetet, vagy mint jelen
helyzetben eset-osztályt, amelynek a fennállása lehetséges és a
fennálása azzal jár, hogy semmi korábbi elgondolásunk és modellünk nem
marad megbízható”. Ebben a helyzetben nincs már értelme arról beszélni
mi bizonyítható. Érdemes tehát nem falszifikálható dolgokkal
foglalkozni? Aki azt állítja, hogy ami nem falszifikálható, az
mindenképpen valami olyan elmélet, aminek a világunkra nincs hatása az
nagyon téved és ez egy költséges mentális tévedés!

*További gondolat 2:* Ez lényegében annak a bizonyítása, hogy minden
bizonytalan? Igen – bizonyos értelemben ezt is mondhatjuk. Ez nem
paradoxon, hiszen ezzel azt állítottuk, hogy nem feltétlenül biztos,
hogy minden bizonytalan… Egyébként pedig ez az eljárás nem azért áll
itt, mert bármi köze lenne a valósághoz. Akinek van egy csöpp esze
**elhiszi**, hogy a valóságban számíthatunk a múltbeli emlékeinknek
legalább a részére – ez a gondolatkísérlet csupán annyit mutat meg, hogy
ehhez hitre van szükség és hogy a hit az egy alapvető emberi
tulajdonság.

*További gondolat 3:* Akkor tehát nincs szükség fizikára,
természettudományokra hiszen nem tártunk fel semmit? Nem, ez nem igaz: a
fent nevezett területek hasznosságát itt nem fírtattuk és nem is fogjuk.
A mindenség megismerésében vannak az ilyen tudományoknak bizonyos
hiányosságai a jelenlegi irányok tekintetében, de a világ megismerése is
legalább annyira fontos, mint az univerzum megismerése. Az itt szereplő
gondolatkísérlet annak a megvilágítására szolgál inkább, hogy milyen
buktatói vannak annak, ha valaki a világról szóló elképzeléseket az
univerzumra próbálja vonatkoztatni, illetve hogy emberként mennyire nem
vagyunk képesek elvonatkoztatni a saját életterünktől még a
tudományokban sem.

<span id="anchor-11"></span>A rózsa színe
-----------------------------------------

Azt mondhatnánk háromféle bizonyítás létezik:

-   Van amit be tudunk bizonyítani mindenki számára és közzé tudjuk
    tenni
-   Van amit be tudunk bizonyítani(magunknak), de nem tudjuk átadni a
    bizonyítást, mert lehetetlen.
-   Van amit nem lehet bebizonyítani, vagy tagadni. Ezek
    falszifikálhatatlan dolgok, de mint azt láttuk, nagyon komoly
    hatással lehetnek az életünkre, vagy modelljeink / módszereink
    helyességére és elfogadhatóságára. Ha olyan elméletet találunk, ami
    mindennel analóg, akkor pedig ha falszifikálható ha nem –
    megtaláltuk az univerzumot helyesen leíró elvet!

Nem sok olyan dolog van, amit mindenki számára be tudunk bizonyítani, de
ilyen például, hogy létezik egyáltalán a világmindenség. Ez mindenki
számára nyilvánvaló, de persze még lehetnek terminológiai jellegű
hitviták! Az, hogy én magam több vagyok mint egy robot, hogy önálló
döntéseket hozok és szabad akaratom van – ez viszont általában már nem
bizonyítható globálisan, hiszen bár én magam részére be tudom ezt látni,
egy robot is viselkedhetne külsőleg úgy, mint én – ő is elmondhatná
magáról, hogy szabad akarattal dönt. Ehhez hasonló probléma-osztály a
nem átadható (de megszerezhető) információk köre.

Egy ilyen nem átadható információt találunk a mindenségben, amikor
ránézünk egy piros rózsára. Ha ketten is vagyünk egymás mellett, bár
elmondhatjuk a másiknak hogy piros színt látunk, de hogy pontosan mi az
amit érzékelünk, azt képtelenek vagyunk megosztani egymással! Ha én úgy
érzékelem a pirosat, ahogy te a kéket és egy fordított, de
össze-egyeztethető színskálával dolgozva megtanultam egy egyes színek
jelentését, akkor is ugyanígy fogok viselkedni. Ez az információ olyan
dolog, ami megoszthatatlan másokkal.

<span id="anchor-12"></span>Az élőlények felépítése
---------------------------------------------------

TODO: Érzékelés = teremtés

Az élőlény, de minden más létező is, egy atomi és pontszerű dolog.
Minden dolog igazából élőlény is, mely azáltal él, hogy új életet ad -
teremt. Az új élet két élőlény közötti kapcsolatból származik.

Minden élőlényre igaz, hogy az összes minden létezőt „látja” és
kapcsolatban áll a teljes mindenséggel, de ez a látás nem olyan látás,
mint amit mi megszoktunk. Akkor ugyanis, amikor a rózsa piros színét
megtapasztaljuk, akkor nem felismerjük a teremtett piros színt – hanem
akkor teremtjük meg magát ezt az érzetet. Az érzékelés maga a teremtés
pillanatában történik meg és ez ugyan úgy teremtés, mint amivel a
mindenséget Isten első létezőként maga is megteremtette. Amikor
kapcsolatot alakítunk ki két létező között (tehát esetünkben mi magunk
és a megfigyelt dolgok között), akkor ennek az eredménye az a teljesen
egyedi és csak hozzánk, illetve a megfigyelés tárgyához kapcsolódó új
létező, aminek mi az előállítását éljük át! A megtapasztalás tehát maga
az élet – és a megtapasztalás egy teremtő folyamat!

<span id="anchor-13"></span>A változás illúziója
------------------------------------------------

A legtöbb, a világmindenségről, vagy világról alkotott modellünk
változással dolgozik. Elemek (pl. atomok, kvarkok, húrok, stb.) vannak a
modellben és ezek komplex rendszereket alkotnak úgy, hogy az állapotaik
„időben” változnak. Állapot lehet például helyzet, vagy viselkedés.
Tehát a modelljeink általában állapotokkal dolgoznak, de ez vajon
valóságos-e, vagy csupán illúzió? Szerencsésebb olyan modelleket
építeni, melyben ilyen boszorkányság nem létezik: ha valami egyszer a
mindenség része lett, az nem változik meg – soha nem kerül egy létező
egy új állapotba. A változás olyan problémákat vet fel, hogy például az
attribútumokat nem létezőként írjuk le, de mégis szerepeltetjük a
modelljeinkben, vagy hogy olyan tereket definiálunk, amelyek absztrakt
dolgokat adnak meg és a létezőkön kívül esnek (ami nyilván botorság).
Ezzel az illúzióval le kell számolnunk, mert tiszta modellben, változás
nélkül is leírhatjuk a körülöttünk lévő közeget.

Ha a változást elvetjük, akkor könnyen adódik, hogy a teremtés, illetve
az új létezők létrejötte fogja megadni azt az érzetet, amivel a
hétköznapi értelemben vett változásokat megmagyarázhatjuk. Ha felállunk
és más szemszögből, állva olvassuk ezt a könyvet, akkor nem a helyzetünk
változott meg, hanem ugyan azt az élményt egy kicsit más szempontból is
megteremtjük és ezáltal mást élünk meg. Amit átélünk azt mind mi
teremtjük, a változás pedig úgy jelentkezik, hogy az élőlények egyfajta
közös megegyezés alapján a megfelelő teremtményeket, a megfelelő módon
értelmezik. Ennek a magyarázata – bár nagyon mélyen rejlik ez a folyamat
a mindenség mködésében – hétköznapi hasonlattal is magyarázható: Hasonló
ez ahhoz, mint amikor a kisgyermek szörnyet lát és megtanítjuk rá, hogy
ezek nem részei a világunknak. Itt a példa miatt, most meg kell
említenünk, hogy vannak olyan irányok, ahol ezeket a dolgokat direkt nem
próbálják a gyerekeknek megtanítani, hogy nehogy elnyomjanak valami
varázslatos médiumi képességet, de lehetőleg mi ne essünk ebbe az
értelmezési hibába. A változás azonban abból fakad, hogy mi akik a
**világot** alkotjuk közösen pontosan úgy viselkedünk, ahogy a világ
kommunikációs modelljében az kialakult. Azok a létezők, amelyek esetleg
más világokat alkotnak, nyilvánvaló módon más rendszert alakítottak ki.

<span id="anchor-14"></span>Álom és valóság, idegenek és más világok
--------------------------------------------------------------------

TODO: mit jelent, hogy ezek is ugyan úgy létező dolgok, mint a
hétköznapi élet – mégis miért nem jelent ez semmi nagyon eget rengetőt?

TODO: A világok közötti utazás problémái – megkötései. Ezek miért
fakadnak csupán logikus korlátokból?

<span id="anchor-15"></span>Az idő
----------------------------------

Az idő alatt túl sok mindent értünk. Ha jót akarunk magunknak, akkor a
modelljeinkben azt választanánk időnek, hogy milyen pillanatok egymás
utánisága adja meg a létező dolgok teremtését és így ezzel a mindenség
mindenkori állapotát. Az időt így, ilyen absztrakt módon értelmezve
paradox módon könnyebben megérthetővé válik, hogy miért is olyan hosszú
a buszra való várakozás – miközben a fizikai mértékegységek szerint nem
hosszabb, mint egy futbalmérkőzés mondjuk.

<span id="anchor-16"></span>Jóslatok, próféciák, apokalipszis
-------------------------------------------------------------

TODO: Miért is van, hogy nem tudhatja senki, hogy mikor következik be?

TODO: Hogyan él együtt a szabad akarat és a jövendölések világa? Hogyan
lehetséges hogy a jövőről lehet dolgokat tudni látszólag, de mégsem
beszélhetünk arról, hogy minden előre elrendeltetett? Hogyan lehet
mindezt modellezni, párhuzamos lehetőségek végtelen létezése nélkül?

<span id="anchor-17"></span>Jövőbe látás
----------------------------------------

TODO: kép arról, hogy mit jelent, ha az átélt idő és a valós idő
eltérnek.

<span id="anchor-18"></span>A jó és a rossz
-------------------------------------------

TODO: A rossz, az tényleg a jó hiánya – így keletkezett, de ettől még
létező entitás, nem csupán üresség! Az, hogy hiány, az a dolgok
megértésében játszik fontos szerepet. Ezért van, hogy nem felelhetünk
rossz tettekkel a mások rossz tetteire és ezért van az, hogy nem
tarthatunk haragot, de azt is így érthetjük meg, miért „hagyja” Isten (a
szabad akarat kérdéskörén túl is), hogy bizonyos rossz dolgok
megtörténjenek. A sátán eme hiányból született és egy létező személy, a
kísértései is létezők, de a természete olyan, akár az üresség
természete.

<span id="anchor-19"></span>A világ teremtése
---------------------------------------------

TODO: ábrák a kezdetekről – a világ olyan mint egy virág, vagy egy nagy
fa ;-)

TODO: kapcsolat a folyamatos teremtésről szóló résszel

<span id="anchor-20"></span>A végtelenség helyes fogalma
--------------------------------------------------------

TODO: A helyes modellekben nem csak változásra nincs szükség, de
végtelen sok létező dolgokra sem. A végtelen sokaság mindig illúzió – a
teremtés folyamatosságának a bizonyos módjait takarja.

<span id="anchor-21"></span>A falszifikálhatóság csapdája
---------------------------------------------------------

Aki túl sokat mozog különféle metafizikai, vagy fizikai körökben az
találkozni fog a falszifikálhatóság fogalmával. Ezt a fogalmat főleg a
materialisták és Istentelen emberek fogják ellened fordítani, hiszen
kiemelik majd, hogy csak olyan elméletek lehetnek mérvadók, melyeket
(természetesen az önkényes módon definiált) tudományos módszerek szerint
potencionálisan cáfolni lehet – tehát ami falszifikálható. Aki nem érti
az univerzumot, az a falszifikálhatóságot keresi, hiszen abban a
csapdában él, miszerint ha valamit nem ellenőrízhetünk bizonyos
tudományos módszerekkel, akkor egyrészt kell lennie ilyen eljárásokkal
ellenőrízhető alternatívának, másrészt látszólag az ilyen elméletek és
elgondolások a felületes szemlélő számára amolyan „nem oszt, nem szoroz”
feltételezéseknek tűnnek. A falszifikálhatóság azonban mint azt
remélhetőleg már megmutattuk nem lényegi fontosságú, sőt! Ha
felfedezzük, hogy azoknak a lehetőségeknek a száma, melyek sehogy sem
falszifikálhatók, de valóságosságuk nagyon is komoly következményekkel
bír igen hatalmas, akkor az is fel fog tűnni, hogy az igazán
falszifikálható dolgok száma megszámlálhatatlanul kevés – olyan kevés,
hogy azt még elmondani is nehézségekbe ütközne.

Nem attól lesz az univerzumról szóló ismeretünk tökéletes, mert ki lehet
tűzni a cáfolatához szükséges kísérlet, vagy tapasztalás lehetséges
módját, hogy majd az egekben reménykedve elvégezzük az így kapott
műveleteket. Nem ettől lehetünk biztos tudásúak, ha a mindenségről valós
természetét akarjuk megérteni, hanem attól, hogy úgy írja le a tudásunk
a teljesés egész mindenséget, hogy nincs oly alternatíva, mely
kevesebbet követelne meg, de mindent megmagyarázna (út ismerete),
illetve egyáltalán nincs is más tényleges értelemmel megtöltött megoldás
(válasz ismerete).

Aki a falszifikálhatóságot keresi, nem a tökéletes megoldást keresi, így
azt nem találja egykönnyen!

<span id="anchor-22"></span>Ami tudvalevő a világról
====================================================

<span id="anchor-23"></span>Törvények és példázatok
---------------------------------------------------

A példázatok mindig többet érnek a szabályoknál és a tiszta erkölcs is
mindig értékesebb a törvények megtartásánál.

TODO: manapság – nagyon bonyolult törvénykezés, több tízezer oldalnyi
jogi szövegek keletkeznek évente, de vajon eléri-e mindez a célját?
Egyszerű és közérthető módon kellene a törvényeket megírni inkább. A
középkorban, ókorban egy paraszt ma meg tudná mondani ki lopott, míg ma
a tolvajok a törvényeket kihasználják. Stb.

Példázatok ereje: Jézus és a próféták is példázatokban beszélnek, mert a
törvények csak tanító erővel kellene bírjanak – viszonyítási pontokat
adnak számunkra. A gonosz ember talán jóvá válik azáltal, hogy megtartja
a törvényeket? A jó tett talán gonosszá válik azáltal, ha valamiért
törvényt szeg?

<span id="anchor-24"></span>Népek, nemzetek és történelmük
----------------------------------------------------------

### <span id="anchor-25"></span>Birodalmak és háttér-hatalmak

TODO: Perzsa, Róma, stb. bábel tornyának újjáépítése – nem tanultunk a
hibáinkból.

TODO: milyen egy „jobb birodalom”? Atilla, Pártus birodalom – nemzetek,
önazonosságuk megtartásával...

### <span id="anchor-26"></span>Kína

TODO: lehet hogy ha csak egy gyermeked van, akkor nem osztod sokfelé a
hagyatékod, de a Kínai kúltúra biztosította be a létezését a földön több
évezredre a jövő generációi számára és nem a holland, vagy svájci
kultúra.

### <span id="anchor-27"></span>Japán

TODO: mit tanulhatunk? A múlt erejének élő példája, a tartás példája.

### <span id="anchor-28"></span>Korea

### <span id="anchor-29"></span>A zsidóság, mint nép

TODO: népről, vagy vallásról kellene beszélni egyáltalán?

TODO: Holokauszt

TODO: bűnök

TODO: pozitív lehetőségek

TODO: negatív lehetőségek

### <span id="anchor-30"></span>Arab népek

### <span id="anchor-31"></span>Dél-amerika

### <span id="anchor-32"></span>Afrikai országok

### <span id="anchor-33"></span>Kihaláshoz közel álló apró népek

### <span id="anchor-34"></span>Angol-szász kultúra

### <span id="anchor-35"></span>Mediterrán típusú népek (Görög, Olasz, Spanyol)

TODO: éghajlati okok és magyarázat

TODO: történelem és különböző kiindulási feltételek

TODO: A délies lazaság előnyei és veszélyei

### <span id="anchor-36"></span>Germán népek

TODO: éghajlati okok és magyarázat

TODO: A precizitás előnyei és veszélyei

### <span id="anchor-37"></span>A török nép

TODO: pre- és poszt-Oszmán/ottomán korszakok.

### <span id="anchor-38"></span>Franciák

TODO: Gallia pusztulásai, „Párizs” neve, bor és szőlő, bölcsesség és
balsors, hibák, multikulti.

### <span id="anchor-39"></span>Fiatal nemzetek és országok

TODO: USA, Ausztrália, erősen multikultúrális nemzetek

TODO: Múlt nélküliség, gyökértelenség

TODO: Baj? Nem baj?

### <span id="anchor-40"></span>Szláv népek

TODO: Sose nézd le a szlávokat – lehet hogy a mindig aktuális birodalmak
szolgaként számolnak velük, de te ne nézd le őket sosem. Ha segítesz
nekik feljebb lépni, olyan nem várt szövetségeseid lesznek, melyekre a
háttérhatalmak soha nem számítanának.

### <span id="anchor-41"></span>A szovjetúnió és az urópai únió

TODO: hasonlóságok

TODO: különbségek

### <span id="anchor-42"></span>Magyarság – a mag népe

TODO: pontosan mit jelent, milyen feladat.

TODO: ne igázz le másokat, a feladatod az, hogy mindenkit kiválasztottá
tégy és ahelyett, hogy hamis történelmet írnak, a sajátjukat, az
igazságot naggyá tehessék, amire büszkék lehetnek! Ne nézd le a
körülötted élőket, hanem próbáld megtanítani arra, hogy

TODO: A magyar nyelv csodája

<span id="anchor-43"></span>Vallásokról és hitről
-------------------------------------------------

### <span id="anchor-44"></span>A szent könyvek helyes olvasata

Meg kell nyilatkoznunk a szent könyvek, szövegek olvasásának módjáról.
Sajnos ahogyan azt mondani szokás: a sátán is képes idézni a bibliából,
de figyelnünk kell arra, hogy ha Istennel keressük a kapcsolatot, akkor
a vallásunk szent könyveit Isten szavaként értelmezzük, tudva azt, hogy
ha valami rosszat olvasunk, akkor a hiba bennünk található! Nagyon
fontos, hogy azt a bölcsességet keressük, ami méltó ahhoz, hogy magának
az Istennek a szájából is elhangozhassék. A szavakat sokféleképpen lehet
értelmezni, de nekünk embereknek megadatott a tudás, hogy el tudjuk
különíteni a jót és a rosszat – csak arra kell nagyon vigyzáni, hogy az
értelmezés során ne vigyük be a szöveg tiszta jelentésébe saját
önzésünket! Amikor valamit önzéssel olvasunk, vagy tanulunk, amikor már
célzottan keresünk a saját szájunk íze szerinti jelentéseket, akkor még
a legszentebb könyvek tartalmát is félre tudjuk értelmezni!

Az Isten-tagadó lelkű ember így talán más módon fogja olvasni a
leírtakat ha azokban a saját igazolását szeretné keresni. Ha valaki
bűnben él és hinni szeretne, akkor esetleg a saját bűneit igazoló
részleteket próbál majd belelátni a szövegbe. Ha valaki egy másik vallás
szent könyvét olvassa, akkor könnyen beleeshet olyan hibába, ami miatt a
másik vallást alacsonyabb rendűnek, hibásnak szeretné látni. Ha valaki
radikális gondolatokat tesz magáévá, akkor akár gyilkosságra való
felbújtást is képes lesz kiolvasni a szavakból.

Az Isten-tagadó emberek gyakran hangoztatják, hogy ezért veszélyesek a
vallások. Ők azért mondják ezt, mert a világképük alapján talán mindent
relatívnak kezelnek. „Mi az igazság és mi a helyes viselkedés, ha nem
egzakt definíció szerint ami egy vallási szent könyvben le van fektetve?
Hogyan lehet akkor helyesen élni, ha nem egyféleképp értelmezhetjük a
leírtakat?” - mondják ők és hasonlítják a vallási kereteket szinte már
az emberi törvénykezéshez.

A hit és a vallás egyáltalán nem a szabályoknak való megfelelés kérdése,
hanem a szeretet és a jóság tanítása. Figyeljük meg az emberi
törvényeket és az emberi ítélkezést: Ki a bűnös és ki a tolvaj? Aki
minden törvényt megtart, de teljes tudatában van annak, hogy
„okoskodva”, a törvény betűjét szó szerint értelmezve kikerülhető a
fenyítés és mégis önös okokból kifoszthat másokat – vagy a kisgyermek,
akinek az anyukája nem mondta még el, hogy a boltban fizetni kell és azt
hitte hazafelé menet betérvén, hogy a csokoládét a polcról csak le kell
venni? A törvény szerint lehet hogy csak a második a bűnös, de Isten nem
egy kiértékelő lapon fogja a mennyek kapujában pontozni a
teljesítményünket, hanem a lelkünkbe látva ismer minket színről-színre.

Itt az ideje a tudatunkba vésni, hogy a szent írásokat, könyveket nem
olyan törvényként adta az úr, amilyen céllal a mi törvényeinket
alkotjuk. Még akkor is tanításként, példaként és magyarázatként kell
tekintenünk rá, ha első látásra törvényhez hasonlítanánk amit látunk. Ne
is próbáljunk teljesen egyértelmű törvényt keresni, mert ezzel a
módszerrel a lényegről feledkezünk meg! Egyszerű és pontos útmutatást
olvashatunk és tanulhatunk a vallásunktól és Istentől – de aki nem
szeretné meglátni, annak nehéz bármit is mindebből megértenie.

A bibliát, más vallások könyveit csak úgy olvashatjuk helyen, hogy
magunkat kiüresítve, Isten igazi példázatait próbáljuk befogadni a
szeretet és jóság keresésével – nincs más útja-módja!

### <span id="anchor-45"></span>A nagy vallások és azok viszonyai

A világmindenség egyetlen egységet képez, a kezdetek-kezdetén jelen lévő
teremtő pedig egy és ugyanaz, bármilyen módon is tekintünk rá, mert
közös és egy a teremtő Isten. Érdemes felhívni a figyelmet arra, hogy
milyen tágas sajnos az, amit már csak az ember gondol hozzá a vallások
tanításához. Példaként az „iszlám” (arabul al-iszlám) - „Isten
akaratának való alávetés” és „muszlim” - „önmagát isten akaratának
alávető”, „Buddha” - felemelkedett / megvilágosodott. Amikor hívőként az
egymás szent szövegeit olvassuk, sajnos mai kontextusban olvassuk őket,
így olyan tévedések és keveredések állnak elő, amiből az egyes vallások
közötti konfliktusok fakadnak – holott az eredeti tartalom szerint és
nyelvi jelentés szerint ilyenről szó sincs a legtöbb esetben! Ezek a
félreértések egészen odáig is elmehetnek, mint az Isten neve: Ahogy
Jézus hívta őt a kereszten „Éli-Éli”, amiben megjelennek a Magyar „él”
és „élet” szavak, de az Arab „Allah” illetve az „al-” szókezdet is az
al-iszlám kifejezésben. Aki felületesen ismeri a hit világát az hajlamos
olyan dolgokat „hinni el”, hogy ezek a nevek annyira különböző
entitásokat takarnak, hogy annyira különbözik az „Allah” szó például a
keresztény „Isten” szótól, mint „Béla” barátunk „István” kollégánktól.
Nem kell, hogy minden vallást és minden tanítást ismerjünk, hogy
mindegyiket ugyan úgy szeressük, vagy valami vallásokon felül álló
szuper-hittel rendelkezzünk ahhoz, hogy mindezt lássuk. Vaknak sem kell
lennünk azzal kapcsolatban, hogy milyen hitnek, mi némely esetben a
gyümölcse, de azzal ha mindezt tudjuk legalább esélyünk lesz mások
megértésére és arra, hogy másoknak az örömhírt terjesszük. A
keresztények is sokszor elfelejtik, hogy tényleg örömhír az, amit
továbbadhatunk. Nekünk nem kell másokat megtérítenünk, hanem már akkor
is boldogak lehetünk, ha sikerül másokkal megosztani az örömhírt. Az
örömhírt, hogy Isten köztünk járt emberként személyesen példát mutatva
és személyes tapasztalatokkal tudja milyen nehéz az élet!

#### <span id="anchor-46"></span>Buddhizmus és egyéb keleti vallások

TODO: Buddhizmus és az univerzumról fentebb mondottak. Leírja a létezők
és a teremtés folyamatát, közös alapot biztosít a szeretettel és
erkölcsökkel. A ponttá zsugorodás eszményét keresi, de ugyanabban a
rendszerben és ugyanazzal a helyes és jó – helytelen és rossz
rendszerben! Lélekvándorlás és reinkarnáció elvei sem feltétlen ütköznek
más vallások elveivel. Ha elolvassuk mit tanítanak mindezekről a keleti
vallások láthatjuk, hogy az ugyan abban az univerzumban meglévő
elképzelésekről van szó, mint amiben a többi vallás elképzelései is
mozognak! Ezekről a fogalmakról is nagyon torzult kép áll előttünk,
melybe jelenkorunkat vetítjük: Nem tanította soha senki, hogy egy
újjászületett például olyan közegbe születik, mint a föld és itt
keletkezik újjá, hanem ezek példázatok, melyek segítettek a kor
emberének megérteni a jelenséget. Ezek a példázatok még a ma emberének
is segítenek megérteni azt, amiről szó van, mert hasonlatok, de itt
sokkal több szabadságot is megenged ez a kérdéskör, ha az univerzumot
egyben tekintjük! Ott világossá válik, hogy nincs „újjászületés”
atekintetben, hogy a létezők egyszer keletkeznek és mindörökké
változatlanok, de az amit átélnek, amit közegüknek érzékelnek azáltal,
hogy megteremtik igenis más lehet, változhat!

#### <span id="anchor-47"></span>Természetvallások, törzsi vallások

TODO: Természetvallások, ausztrál bennszülöttek, ezoterikusnak tűnő, de
régi törzsi vallások. Mind érzékelnek részeket a mindenség működéséből.

#### <span id="anchor-48"></span>Materializmus mint vallás

TODO: a materializmus / „nem-hit” mint vallás – miért is tekintendő
vallásnak?

#### <span id="anchor-49"></span>A zsidó vallás

TODO: Zsidó vallás – Jézus szerepe, elkövetett bűnök, látszólagos módon
el nem érhető helyes út, mely mégis teret ad a helyes útnak! A
kiigazításra szorult vallás, mely helyesen követve továbbra is a jó
szolgája lehet – de nagyon veszélyes közegben létezik. Mindezt
kifejteni!

#### <span id="anchor-50"></span>A kereszténység

TODO: Sajnos sokan félreértik és felületesen ismerik… Az örömhír
nagysága sokkal fontosabb bárminél

#### <span id="anchor-51"></span>A tisztítótűz

#### <span id="anchor-52"></span>Pokol és bűnök, megbocsátás fontossága

Jó emberként élni nem csak a pokoltól való félelem miatt „kell”, hanem a
kereszténység igazi tanítását akkor értjük igazán, ha akkor is jók
szeretnék volna maradni, ha már tudnánk és Isten személyesen mondta
volna el, hogy a pokolra kerültünk! Ez a keresztényi gondolat, nem a
pokoltól való félelem, hanem a jó tett önmaga jutalmaként történő
működés gondolata!

#### <span id="anchor-53"></span>Javaslatok és álmok

TODO: A legontosabb – minden vallás az egy Istent és teremtményét
imádja. Nézzük csak meg amit fentebb az univerzumról mondtunk!

TODO: Közös esküvés módja, eltérések feltételes elfogadása: „ha igaz,
akkor elfogadom”. Aki mélyen hisz, annak ez nem sértő!

### <span id="anchor-54"></span>Ezoterikusok, szekták és titkos társaságok

Sokan vannak, akik azért keresik a varázslatot, mert a világból el
szeretnének menekülni és el szeretnének bújni különféle dolgok elől. Az
ezoterikumok legtöbbje is valami hasonló keresésen alapuló tévelygés.
Nem minden szekta és ezoterikus forrás tekintendő káros kicsapongásnak,
de ahogy azt Jézus is példabeszédeiben tanítja: minden a gyümölcséről
ismerszik meg. Nagyon fontos, hogy valaki az élő Isten-t keresi-e, vagy
hogy vajon nem-e igazából csak a saját ego-jának megfelelő
fantáziavilágot szeretné kivetíteni a valóság helyében. A világmindenség
a mágián alapszik, mag-elvű, de mégsem érdemes a varázslatok útját
járni. Ez nem azt jelenti, hogy el kell vetnünk a természet olyan
vonásait, melyek misztikus erők jelenlétét mutatják, hanem azt jelenti,
hogy minden ilyen dologgal kapcsolatban úgy kell eljárnunk, ahogy egy
igazi mágusnak el kell járnia. A mágia és a hit nem varázslás, sem nem
kufárkodás. Akik haszonelvűen pénzt, előnyt csinálnak a vallási
dolgokból, vagy a tartalom helyett a rituálékat részesítik előnyben
majdnem mindig tévelygő bálvány-imádóknak, illetve rosszindulatú
embereknek tekintendők! A legnagyobb balszerencse a sok szerencsétlenség
között, hogy bizonyos alkalmakkor ezek a félresikló módszerek akár jól
is sikerülhetnek, be is teljesedhetnek. Ez a legtöbb esetben sajnos nem
azért történik, mert a drámaian előadott „energiák”, „szellemek”, vagy
egyéb entitások és a kitalált mesék helyesek, hanem vannak esetek,
amikor mindez úgy van összhangban a mindenség és a lélek szabályaival,
hogy ez az megfeleltetés mellékhatásként megteremti a kívánt eredményt!
Hasonlatos ez ahhoz az eseményhez, amikor valakit szerencse ér egy
játékban, de rossz stratégiával játszva éri azt el, vagy ahhoz a
személyhez, aki felvételt nyer egy pozícióba a beszéde miatt, de később
kiderül róla, hogy a tettei nem teszik alkalmassá a feladat
betöltéséhez. Óvakodjatok hát attól, hogy bárki ezoterikus
gondolkozásúnak tartson titeket. Amit ti képviseltek az nem rejtett
tudás és nem a világ elől való menekülés. Nektek a világon belül kell
megtalálnotok a magvetői szerepet, különben nem magvetők, hanem
megvetettek lesztek! Ne féljétek a tudományokat, mert nem ellenségeitek
azok! A tudományok és a természet ismerete nem csak a feladataitokban
segít majd titeket, hanem mint az remélhetőleg kitűnik a mondottakból
ezek a defenzív típusú mágia erős eszközeiként is tekinthetők. Nem a
véletlen műve, hogy mindazok, akik érzelmi, neveltetési, bűnbeli vagy
más okból elbújnak Isten elől, nagyon sok alkalommal a tudomány
különféle eredményei mögé próbálnak bújni. A siker természetesen
látszólagos és ez a folyamat rájuk nézve káros, mely a sátán
manipulációja, amiből a halállal egy csapásra ki lehet szakadni, hogy
mindenünket elveszítsük, de az elvetett különböző negatív magokat,
mágikus behatásokat azáltal kontrollálni vagyunk képesek valamilyen
fokig, hogy bizonyosak vagyunk benne meddig tartanak a világ korlátai.
Akárcsak egy emberi kapcsolatban, itt is nagy szerepet játszanak a
határaink. Akárcsak egy kapcsolat esetén, ha biztos határokat alakítunk
ki magunk körül, akkor sokkal kevésbé ingat minket a kísértés és a bűn.
Aki ezt a biztonságot elveti nem bölcs az, de az sem bölcs, aki vallást
alakít ki a világ szabályai körül. A világ nem azonos a mindenséggel,
így ne essünk a XX. században fénykorát élő, de kihunyó materializmus
csapdájába sem és annak ellenére, hogy felismerjük Istent és magvetői
szerepünket, ne a varázslatot, hanem a jó dolgokat, a jó tettek
lehetőségét keressük a világban!

Az ezoterikus gondolatokra építő média, sarlatánok és világ elől
menekülő tömegek, viszonylag békés körein túl már sokkal nagyobb
problémát jelent a különféle szekták körke. Ezek is a gyümölcsükről
mutatkoznak meg és bár nem feltétlenül mind rosszindulatúak, kellően
veszélyesek ahhoz, hogy megfelelő távolságot tartsunk tőlük. Ha azt
látjuk, hogy a gyengékre vadásznak, de erősként nem kellünk nekik, vagy
ha világi elvárásokat támasztanak feléd úgy, hogy annak nem világos a
szerepe, akkor veszélyben vagy. A szellemi életet meg lehet élni
vadhajtások keresése nélkül is! Legyen bármilyen eredeti vallásod, vagy
bármilyen neveltetésed, azt keresd ami a világ világossága! A rituális
szertartások másodrendűek és csupán a tartalmat hivatottak
megvilágítani, a helyes életmódot segítenek megélni. Ha egy szekta a
rítusok köré szerveződik és álmodozók vezetik, akkor vakok vezetnek
világtalan és süket embereket, ha pedig saját nagyméretű költségvetéssel
és pénztügyi szabályokkal dominálnak benne, akkor azokkal a
gonosztevőkkel van dolgod, akik maguk sem hisznek abban, amit
prédikálnak. Ha egy mód van rá ne keressétek a szektákat és ne is
alapítsatok szektákat még akörül sem, amit ebben a könyvben olvastok! Ha
meg szeretnétek beszélni élményeiteket, tanítani akarjátok egymást a
világ és a mindenség dolgairől, a magvetés módjairól, akkor járjatok
össze és beszéljetek, ha kell olvassatok könyveket közösen, vagy
tegyetek közösen lelki kirándulásokat, de ne alapítsatok vallási
szektákat, mert mindezek az embertől származó vallások távolabb visznek
titeket az egyetlen Istentől és a mennynek országától!

Az ezoterikus világon és a szektákon túl meg kell még említenünk az úgy
nevezett titkos társaságok körét is. Ezek a társaságok összeesküvés
elméletekhez kapcsolható ilyen, vagy olyan célból létrehozott csoportok
amelyek valamilyen titkos módját választják a létezésnek. Ezzel
kapcsolatban azt kell tudnunk, hogy akinek nincs mitől tartania és nem
fél a haláltól sem, az nem szeret titokban maradni és nincs oka rá, hogy
titokban maradjon, amely ilyen szervezetek léteznek, azok viszont
örömüket lelik abban, hogy lejáratható összeeküvés-elméletek alakulnak
köréjük, illetve még náluk is hihetetlenebb titkos társaságokat találnak
ki az emberek félelmükben, hiszen ezzel egyfajta védelmet nyernek
mindazokkal szemben, akik kritikusan is képesek a dolgok mögé nézni és
nem csak a szenzációt keresik. A világban nem csak a jó, de a gonosz
erői is jelen vannak és ezért megeshet, hogy bizonyos dolgokat nektek
sem szabad nyilvánosan tennetek, de hosszú távon és célként az ilyen
típusú létezés tarthatatlan egy igaz, jó szándékú magvető számára! Ne
alkossatok hát titkos társaságokat a világ jobbá tételére, mert a
világunk a benne élő emberek jóságának növekedése miatt fog jobbá válni,
akiket titokban nagyon nehéz lesz megszólítanunk! Alkalmazzátok a titkot
ahogyan azt jól lehet alkalmazni: túlélésre, rejtett magvak elvetésére
és azért, hogy másokat meg ne botránkoztassatok, de ne váljon sose céllá
a titok megléte, vagy a föld alatti működés! Mindemellett tartózkodjatok
minden titkos társaságtól, különösen azoktól a legveszedelmesebbektől,
melyek nem az emberek jobbá tételével, hanem az emberi gyarlóság
kiszámíthatóságával foglalkoznak! Ha az emberek vezetésének módja a
rossz tulajdonságok kiszámíthatóságán alapul, akkor máris feladtuk a
sátánnal szembeni harcot, így még jó célok esetén is a sötétséget
támogatnánk – méghozzá annak a legveszélyesebb módjait! A titkos
társaságokat mindezek mellett ne is keressük és ne akarjuk „leleplezni”.
Nem ez a velük való küzdelem módja! Ezek a társaságok kivéreznek,
mihelyt az emberiség bölcsessége megnövekszik – amelyek pedig ki nem
véreznek, önként felfedik majd magukat hiszen rejtőzködésük okafogyottá
válik! Mágusként számíthattok arra, hogy lesznek akik titokban rejtőztek
és majd veletek tartanak, de ne legyetek naívak, mert olyanok is
lesznek, akik csak barátaitoknak mutatják magukat, de titokban a sátán
útját járják! A bölcs ember, nem számol titkos segítőkkel miközben magot
vet a jövő számára, így minden segítség úgy éri majd tervét, hogy csak
annál jobban biztosítja sikerünket!

### <span id="anchor-55"></span>Jézus, a próféták, szent emberek és a kinyilatkoztatások kora

Sokan kérdezik maguktól, hogy Krisztus, vagy akár a különböző próféták
miért akkor jöttek el az emberek közé, amikor eljöttek és miért nem
akkor, amikor például létezik már tömeges média, televízió, internet,
újság, vagy egyáltalán fényképezés. Nem csak azért lehet jelentősége az
időpontnak, mert utólag abban se lehetünk biztosak, hogy nem-e pont
azért létezik még egyáltalán emberiség, mert ezek nélkül az események
nélkül már rég a pokolban sínylődnénk, így se televíziónk, se jövőbe
tekintő pozitív víziónk se lenne, hanem azért is, mert paradox módon
pont így tud még egy Isten is sokkal hitelesebb lenni. Az Isten
mindenhatóságát nem ássák alá az univerzális törvények, de néhány dolgot
nehéz megértenünk annak a módjával kapcsolatban, ahogyan ez a mindenható
jelleg a világban ki képes bontakozni. Nem véletlen ugyanis, hogy egy
jelnek, csodának, vagy akár az ártó jellegű dolgok megjelenésének is van
mindig „természetes” magyarázata is – még ha az esetleg nehezen is
hihető. Ahhoz, hogy egy angyal a mi világunkban jelenjen meg úgy
szükséges megjelenési módot választania, hogy azzal számunkra is
látszódjék. Ez bizonyos értelemben még Istenre is vonatkozhat talán, de
nem azért, mert nem lenne képes másként megjelenni, hanem ha úgy
jelentkezne a mi világunkban, hogy mindent amit ismerünk felborít – nem
pedig emberi alakban, vagy apró jelként – akkor azzal mintegy az
armaggeddont hozná el nekünk. A világot a hit és a közös lelki platform
tartja egyben és az különbözteti meg a mennyországtól, pokoltól, vagy
akár az olyan egyszerűbb világon (félig már) kívül eső elemektől, mint
egy álom, hogy közös szertartásos alapot biztosít és minden szereplő
ezen protokolláris szerepkörben mozog. Ha valami nem illeszkedik bele
ebbe a szabályrendszerbe, akkor az jobb esetben talán még álomként, vagy
valami absztrakt dologként jel gyanánt megjelenhet, de minél távolabb
van valami attól, ami a világot alkotja, a megjelenése annál inkább vak
szemekre találna. Ha ilyen feltételek mellett Istent személyesen
meglátjuk, akkor kieshetünk, kiszakadhatunk a világunkból és őrültként
jelenhetünk meg, vagy rosszabb esetben akár tömeges esetben a világ
végét is el tudná mindez hozni talán, míg ellenkező esetben az eseményre
teljesen vakok lennénk és nem is tudnánk arról, hogy bármi is lezajlott
körülöttünk. Nagyon könnyen lehet, hogy ma már annyira megfigyeljük a
világunk működésének minden kis részletét és annyira szerteágazó
mélységben határoztuk már meg a közös alapként szolgáló szabályokat,
hogy csak sokkal rejtettebb módon – vagy pedig sokkal nyíltabban és a
világ törvényeit megrendítve – tudna közénk a megváltó eljutni úgy is,
hogy mi is lássuk őt. Ez nem az ő erejének a korlátja, hanem mi vagyunk
egy olyan tévképzet fogjai, amelynek rabjaként csak bizonyos módokon
lehet velünk kommunikálni. Ez a probléma a mi korlátoltságunkból fakad
és annak ellenére, hogy így is felodható, nem tudhatjuk, hogy ennek a
feloldása mivel járna. Egy festőművész is rendelkezik teljes
szabadsággal atekintetben, hogy a vászonra mit és hogyan fessen fel,
mégis – annak ellenére, hogy bármit meg tudna tenni, azért olyan kép
festését választja, melyet a közönség is értelmezni képes! Mégsem
mondjuk azt, hogy a közönség korlátozza a festőt abban, hogy mit tehet
és mit nem. Ehhez hasonlatos módon az Isten sem veszíti el
mindenhatóságát azáltal, hogy végtelen bölcsességében talán olyan
módokat választ az emberekkel való érintkezésre, mely sokkal jobb annál,
amint amit mi esetleg magunk a legerőteljesebbnek elképzelnénk!

<span id="anchor-56"></span>Populáris kultúra és média
------------------------------------------------------

<span id="anchor-57"></span>A lokalitás törvénye
------------------------------------------------

<span id="anchor-58"></span>Tudomány
------------------------------------

A tudomány ne legyen ellenséged! Légy tudóssá, ismerd a világot, ismerd
meg a fizikát, matematikát, biológiát csak ne ess ábrándok fogságába. Ne
hagyd hogy az, ahogyan hitetlen emberek a saját igazolásukra próbálnak
menedéket keresni a részletekben való elveszésben, téged megakadályozzon
a tudományok művelésében!

A tudományok világában találkozhatunk sok önzéssel, önteltséggel és
Isten iránti, vagy akár a nemzetek és értékek iránti haraggal is, de nem
a tudás, hanem az emberek által találkozhatunk csak ezekkel. Nincs
félnivalónk: Istent nem fogjuk elveszíteni azáltal, hogy többet tudunk
meg a körülöttünk lévő világról. Ha a lelkünk nem zárkózik el Istentől,
akkor az is világossá lesz, hogy az olyan nézetek miszerint tudós ember
nem lehetne hívő csak néhány szűk ateista kör által terjesztett
hazugság, amivel saját felsőbbrendűségüket igyekeznek igazolni. Amikor a
tudományokkal foglalkozunk, akkor tényleg meg

<span id="anchor-59"></span>Emberi- és párkapcsolatok
-----------------------------------------------------

### <span id="anchor-60"></span>Szeretet

Szeretet nélkül semminek nincs értelme se a világban, se az
univerzumban. Az különbözteti meg az igazakat és a Magyart a sehonnani
bitangoktól, hogy nem mond ellent a szeretetnek, nem engedi elveszni a
világban és a mindenségben meglévő szeretetet és teljes szívvel őrzi
azt. Az igazi mágia, magosság szükségessé teszi ellenfeleink,
rosszakaróink szeretetét is ahogyan azt Jézustól tanulhatjuk. Aki dühből
és haragból cselekszik – nem mágus az és nem cselekszik magosságot,
hanem csak ösztöne vezérli. Sok olyan, hazafias és az értékekért kiálló
embert fogunk találni, aki így tesz, de az igazak még rövid ideig se
teszik félre a szeretet törvényét, hiszen ez szövi át kultúránkat. A
szeretet nem köt béklyóba, nem akadályozza az önvédelmet, nem dönt
nyomorba, hanem még a küzdelmek elvesztésekor is felemel úgy, hogy egy
olyan nép, mely a szeretet törvényét követve hal ki, évszázadokkal
később újjáéled abban a pillanatban, amint felfedezik tettében dicsó
múltját és utánozni kezdék tetteit.

A szeretetben, mint a neve is mutatja „szer” rejlik tehát, az igazi
szer, amit az emberek az élő Istennel való szövetség mintájára kötnek,
olyan kötelékké, amely az igaz ember minden cselekedetét átszövi és
megfelelő, magosságos „tett”-eket szül.

### <span id="anchor-61"></span>Szerelem

A szeretet magosságos törvényét alkalmazók remélhetőleg maguktól,
önmaguktól – tehát abból a magból, mely bennük él és csírázik ki a
világra – is igaz módon állnak a szerelemhez, de mindenképpen el kell
mondanunk rövid tanácsainkat erről is.

A szerelem nagyon szép, de virága úgy emelkedik fel, ha nem csupán
érzésként, vagy érzéki élményekként, hanem annál sokkal mélyebben éljük
át. Csakúgy, mint a szeretet tartalmazza a „szer” szótövet is, azonban
itt egy olyan fontos szerepben jelenik meg, ami a szerelmes
mindennapjainkat is megvilágítja, igazzá teszi. A szerelem nem csupán
érzelem egy mágus számára, hanem egy olyan szer, amiért bármit képes
lenne feláldozni. Akit szerelmünknek választ a lelkünk, azzal lelkünkben
egy olyan szer részévé válunk, aminek a szerelem leginkább nem a
megélése, hanem az alkalmazása. Ezért magosságos az a hűség, mely kitart
tűzön és vízen keresztül, ezért gyönyörű úgy állni a szerelembe, hogy
nem magunkért, hanem a másik fél értékei és érdemei miatt, szerelmes
szövetségesként akarunk a párunk mellett állni. A sátán arra csábít
majd, hogy a szerelemhez hasonló, de az önzésből származó érzelmeket
tápláljunk. Nem szerelem az, amely értünk van. Az igaz szerelem, onnan
ismerszik meg, hogy a szerelmünk tárgyáért van bennünk meg!

Óvakodjatok mindazoktól, akik a szerelemben és párkapcsolatokban is az
egó szerepét, vagy a birtoklást szeretnék megjeleníteni! A modern
világban olyan nézetet igyekeznek kialakítani, hogy ildomos párunk fölé
emelkedni, mert mind a nők, mind a férfiak az erős embert keresik a
másikban, így ha nem birtokoljuk partnerünket, akkor bizonytalanságban
élünk. Boldogtalanok, akik így gondolkoznak és még nem tanulták meg az
igaz szerelem képességét. Aki igaz szerelmet bír, felemeli párját akár
maga fölé is.

A szerelmeteket ne adjátok könnyen! Ne tartsátok magatokban, de legyetek
erősek! Nem attól erős valaki, hogy másokat uralni próbál, hanem abban
mutatkozik meg az ember ereje, ha értékeiért lélekben képes tenni és ha
kősziklaként áll a fúvó szélben is! Ne legyetek önteltek, de ha valaki
nem fogadja be a szerelmeteket, akkor ne sajnáljátok – olyan párt
találtok majd, aki jobban megérdemel őnála, de ha nem is találtok, akkor
is igazak lesztek.

Keressetek folyamatosan és imádkozzatok! Annál nincs nagyobb öröm, mint
amikor imátok meghallgatásra kerül és találkoztok azzal, akinek ti
jelentitek a nap sugarait, az élet koronáját és így ti megadhatjátok az
igazi boldogságot párotoknak! Akit nem az igaz lélek, hanem a birtoklás
lelke vezérel, ilyenkor felbátorodik és valami még tökéletesebbet, még
jobban a mi önző énünket csillogtatóbb személyek után kezdünk áhítozni.
Ezt a hullámvasutat ne vállaljátok fel, mert nem az igaz szívűek útja
ez, hanem a gyenge és sérült lelkeké! Legyünk hűek lelkünk választásához
és elérjük a boldogságot. Aki így cselekszik kincset talál, pedig minden
kincsét odaadná a másiknak – ezzel szemben cselekedni viszont olyan
éretlenség, amely csak bajt hoz ránk és környezetünkre. Ha valaki
szerelemmel érez felénk ne féljünk közeledni, mert ez nem az
alsóbbrendűségének a jele, hanem a szív őszínte üzenete. Vagy tán akik
felé szerelmet éreztünk viszonzatlanul nem vesztették-e el a világ
világosságának hősét, táltos mesék mondabéli urait és lányait? Ne
féljünk azoktól sem, akik jó fiúknak, és rendes lányoknak tűnnek, mert
olyan nincs, amit mi nem tapasztalhatunk, de olyan mindenképpen van,
amit csak velük tapasztalhatunk a legszebben.

Éljünk olyan szerelemben, amiben minden nap kedvvel imádkozunk
szerelmesünk lelki és testi épségéért, azért hogy mindörökkön örökké s
még azután is ebben a szerelemben lehessünk. Aki olyan embert lát meg
szerelméül, ki ezt viszonozza, az a világ szerencsése! Aki ilyen
párkapcsolatban éli házasságát és életét, kősziklára épült,
lebonthatatlan kastélyban lakik, ahol szíve kedvesével naponta táncra
kelnek magukban, míg testük-lelkük össze nem nől teljességben!

Mivel a szerelem és szeretet a legfontosabb mágia a szó magosságos
értelmében, így egy kicsit összetett példát is érdemes itt most adni
arról is, hogy milyen az igaz szeretelem! Az igaz szerelemben élő ember
lelke képes kimondani magában a következőket: „Ha a sátán elválaszt
minket, akkor olyan szent életet élek, hogy az elmondhatatlan. A párom
annyit ér, hogy ha a kígyó szétrombolja a kapcsolatunkat, az
értékeinket, akkor érte én képes vagyok tényleg fel is adni magamat,
egyenesen Istennek élni és már itt a földön olyan szentséges életben
maradni, hogy ezzel a sátánon háromszor – sőt harmincháromszor, is
visszaverje az ő undorító tetteit mindaz, amit hatására tisztán
cselekedek majd. Tudom, hogy a szerelmem még ennél is többet ér ezért
azt is tudom, hogy az Istenem nem nyerne ezen, de tudja meg ég és pokol:
én soha nem esek el, engem soha meg nem törhet a gonoszság lelke!”.
Fontos, hogy olyan emberrel ejtsük szerét a szerelemnek, hogy ezeket
tényleg így mondhassuk el saját lelkünkben. Emiatt van, hogy nem fogunk
eltávolodni egymástól és nem fogunk sem elválni, sem megkeseredni, de
még ha párunk verni is kezdene, vagy részegeskedne, akkor is úgy
lehessünk hűek igaz döntésünkhöz, hogy saját magunknak – tehát annak a
magnak, mely bennünk a fény ígéretét tartalmazza – ne ártsunk! Nem lelki
önsanyargatók, hanem igaz emberek legyünk és igaz embereket keressünk. A
saját érdekünk és az emberiség érdeke is, hogy ne adjuk alább!

### <span id="anchor-62"></span>Lovagiasság

Vannak akik a lovagiasságot úgy élik meg, mintha a lovagiasságot csak a
gyengével szemben gyakorolhatnánk. Nincs is nagyobb ostobaság ennél a
gondolkozásmódnál! Ha valaki lovagias, akkor ne elutasítással, hanem
felemelkedéssel háláljuk meg! Ha valaki megkímél egy kellemetlenségtől,
vagy segít, akkor ne érezzük magunkat gyengének! Érezzük inkább azt,
hogy eme cselekedet hű hálájaként az így megmaradó szabad mozgásterünket
és kényelmünket arra használjuk fel, hogy még csodálatosabb dolgokat
cselekedjünk! Boldog az a nő, aki képes elfogadni a lovagjának
lovagiasságát, de nem önző éne miatt élvezkedik benne, hanem szeretetből
és szerelemből éli meg ezt a fajta kapcsolatot de ostoba ellenben az
olyan férfi, aki dühöt táplál maga és a világ felé olyan pillanatában,
amikor nem érez elég erőt a lovagiasságra.

### <span id="anchor-63"></span>Segítségnyújtás és a segítő kéz elfogadása

TODO: Nyújtsunk és fogadjuk is el a segítséget!

### <span id="anchor-64"></span>Szeretteink elvesztése

TODO: Halál, vagy más által történő elvesztés is (pl. válás, földrajzi
távolság)

<span id="anchor-65"></span>Szexualitás
---------------------------------------

A szexualitással mindenki találkozik, része az életnek, természetnek és
a boldogságnak. Ezért van, hogy a populáris kultúra és a háttérben
megbúvó erők egyik legnagyobb pusztítása pont a szexualitást éri. A
mágusnak látnia kell mi helyes és helytelen, mi mögött húzódnak meg
érdekek és hogyan kell ehhez a témához bölcsen viszonyulni.

Rögtön le is kell szögezzük: nem az önmegtartóztatás minden bölcsesség
kulcsa, nem is a különféle szabályoknak való megfelelés. Ezt azért
fontos már most egyértelművé tennünk, mert amikor az anyagi,
materialista világ porrá hamvadó követőinek elavult gondolkozásmódjával
kell ezen a területen küzdenetek, akkor ők elsősorban bigottsággal,
elfojtással fognak vádaskodni – még annak ellenére is, ha világosan
fejtitek ki véleményeteket! Az ellenségeitek és a tudatlanok stratégiája
az szabadosságra, az egóra, az emberi gyengeségekre és a félelmekre
fognak hatni, nektek ezeket kell kivédenetek mind magatokra, mind
másokra vonatkozólag!

A XX. század az erkölcs nélküli világ és a tévedések világa: annak a
kísérlete, hogy mindent aminek komoly értéke van, megfosszanak
lényegétől és lelkétől. A szerelmeskedés és szex ennek megfelelően úgy
anyagiasodott el, akárcsak a jó bor élvezetét változtatják át ahol csak
lehet igénytelen részegeskedéssé és érték nélkül italok, mérték nélküli
fogyasztásává. Mindezek ellenére a fény kihunyását nem a mélyebb orgiák,
vagy a szabályok elvetése – hanem ezek általános eredménye a
személytelenség és lélektelenség okozzák! Ellentmondásosnak tűnhet majd
a tény, de a XX. század igaz szemmel nézve a szexualitás ellen zajlik:
Látszólag mindenhol és minden időben találkozunk vele, de eközben a cél
igazából az, hogy sehol és semmikor ne találkozhasson a ma embere a
legnagyobb csodákkal és ez frusztrációt, gyengeséget – így pedig
irányíthatóságot okozzon! A pornó, a mindenhol megjelenített aktusok
professzionális ismerete és megfigyelése nem tapasztalttá teszi az
embert, hanem gyengévé. A világot úgy állítják be számotokra, hogy ha
lehet ne merjetek cselekedni, ne merjétek ti tenni meg az első lépést,
hogy azt érezzétek elvárások nehezednek a vállaitokra. A legtöbben meg
fogjátok tapasztalni az érzést, hogy tapasztalat nélkül értéktelenek és
meztelenek vagytok, a médiában, a filmekben az elutasítás szerepe jut
mindenkire aki nem követi a háttérben megalkotott társadalmi normákat.
Tudnotok kell, hogy ez az érzés nem saját magatok tulajdona, hanem egy
terv része, tudnotok kell, hogy ez az érzés korábban – az „elfojtások”
és „bigottság” világában – nem, vagy sokkal kevésbé létezett! Na ez az,
amit nem reklámoznak a sorozatokban és elfelejtenek. Ne legyetek
bigottak, de szabadítsátok meg a bennetek élő lelket minden
szégyenérzettől és félénkségtől: Amit éreztek az egy globális játszma
része, de ha ellen tudtok álni a nyomásnak, sose féltek csak reméltek és
kemények lesztek akár egy igazi harcos, akkor azok közé a kevesek közé
tartoztok majd akik tényleg igazi férfiak és igazi nők.

Aki úgy véli, hogy a szexualitás érték nélkül, vagy fontosság nélkül
való dolog, az nagy és veszélyes tévedésben él. A szexualitás nagyon
nagy értéket képvisel, de ennek az értéknek a megjelenési formája nem az
átélésben manifesztálódik, hanem az átadásában. Ha bárkinek átadod az
intimitásod, bárkit ily módon magadhoz fogadsz, akkor nem ér semmit,
amit adni tudsz. Ha valamely perverzió, vagy a szexualitás vezet, nem
pedig az, hogy a másik fél számodra kiválasztotti rangot képvisel, akkor
nem ér semmit, amit adni tudsz és bűnt követsz el. Ha egyszer eljön az,
akiért képes lennél akár az életedet is feláldozni, akkor elég egyszer
is elvesztened ártatlanságodat, mindörökkön örökké siratni fogod, hogy
megtetted. Sajnos még az erényes is megtévelyedik. A gyarló világot
látja maga körül úgy, hogy semmi esély, semmi remény nem fénylik fel
évekig aztán végül kínjában „gyakorolni” kezd olyannal, aki számára nem
jelent semmit, vagy akiről nem tudja mit jelent a számára. A szex nem
számít, csak a szerelem – mondja az ilyen ember és elköveti a tettét. A
szeretkezés tényleg nem cél, hanem egy nagyon jó mód a szerelem
kifejezésére, de nagyon is számít! Soha ne értékeljük le magunkat és
soha ne válasszuk szét a szerelmet, szeretetet, tiszteletet és a
szeretkezést!

Nem kell bűntől mentesen élnetek, de akárcsak minden másban, ebben is
csakis a valóságot kapjátok, minden feleslegesen hosszúvá váló
magyarázat nélkül. Ne féljetek, mert a félelem a sötétség legnagyobb
eszköze! Sose féljetek semmitől, mert akkor lesztek igazi férfiak és
igazi nők ha adtok magatokra. Ha a legmélyebb perverziókban van részetek
se emelkedjék felül a vágy személytelen módon és még akkor is
törekedjetek értéket képviselni úgy, hogy a veletek való együttléthez
kiválasztottnak kelljen lenni. Ha mindezt megtartjátok, ha ahelyett,
hogy különféle filmszereplők stílusát utánoznátok igazi bátorsággal,
saját magatokat, félelem nélkül stabilan adjátok, akkor olyan párotok
lesz, aki tényleg kiválasztotti minőséggel bír. Az igazi férfi nem fél
az elutasítástól, ő sajnálkozik, ha egy nő mindezt nem érti meg. Az
igazi nő mindezt megérti és nem ostoba: nem keres előre megszabott módon
szeretőt, hanem minden pillanatát élvezi amit nyújtanak neki. Az igazi
párkapcsolatban bárki hibázhat szeretkezés közben és senki nem fogja a
másikat hibáztatni, mert számára a másik fél egy két lábon járó csodával
ér fel. Ha nem féltek a hibától, akkor nincs görcs az emberben, így a
legjobbat vagytok képesek nyújtani.

Aki a rossz nőt kívánja, általában nem tudja igazán mi a rossz. Aki a
rosszfiúkat szereti az legtöbbször nem tudja milyen aki igazán rossz.
Aki nem képes valakit önmagáért szeretni, hanem eljátszott szerepek
miatt szereti a másikat, az nem szereti a másikat. Az intimitásban nem
tiltások, hanem példák vezessenek. Az ostoba ember a példákat
megszorításnak értelmezi, holott ezek az ember javát szolgálják. Amikor
hibát követsz el és visszatekintesz, akkor megérted őket. Az igazi bölcs
akkor is megérti mindezeket, ha nem követ el hibát és átlát az
ellentmondásokon: mindaz ami itt írva van semmitől sem foszt meg
titeket, hanem abban igyekszik példamutatást adni, hogyan tudtok értéket
adni annak, ami az intimitással kapcsolatos. Bármilyen vágyakkal is
rendelkeztek, arra mindig van mód, hogy keressétek az értékeset. Ha
rátaláltok, akkor a szemébe nevethettek bármely szabadelvűnek és
tévelygőnek – ha valaki megérti mindazt, amit mondtam, akkor tudja hogy
a szabadosság az egyetlen módja annak, hogy olyan csapdába essünk,
melyben valami elérhetetlenné válik számunkra. Azért igyekeznek mindent
kipróbálni egyesek, mert vannak olyan dolgok, amiket mindörökkön örökké
elveszni látnak már.

<span id="anchor-66"></span>Kísértés
------------------------------------

Légy erős, ne lengedezz,\
Amerre épp szél fúj.\
Hős erővel, úgy eredj el,\
Kisértőtől, gonosztúl!

Ostobaságtól távol állj,\
Hited ne hagyd oda!\
Vesszen el az ostoba,\
De te ne légy buta!\

<span id="anchor-67"></span>Alkalmazandó stratégia
==================================================

<span id="anchor-68"></span>Az ellenség
---------------------------------------

Ellenséged nem emberi személy,\
hanem oly ördög, ki az emberben él!\
Nem nemzet ez, vallás, vagy nép,\
De sátán, ki maga pusztítás, megtévedés!\
- Az embereket bántván ő csak tovább nő, tovább él!

TODO: kik és milyen fegyverekkel, milyen módon? Emberekről beszélünk,
vagy emberekben élőkről? Miért is szeressük ellenségeinket?

TODO: Ne felejtsük el, hogy ha elég magas szinten űzzük a mágiát, akkor
eltörölhetnek a föld színéről és mi még a Románok ivadékai közül is
Magyarokat támaszthatunk ezer év múlva, míg azok, akik minket
elpusztítani igyekeznek, erre képtelenek és így halandók! Nem azt
jelenti Magyarnak lenni, hogy vér szerint, vagy magasabb rendűség
szerint valakik felett állunk, hanem hogy úgy kell cselekednünk, hogy
másokat is a saját nemzetük, kultúrájuk és az igazság szeretetére
neveljük. Hogy segítsünk még a nehezen elfogadható igazságban is
megtalálni számukra a megnyugvást és hogy érezzék: nem félniük kell,
hanem barátként tanulnunk egymástól még ha ártottak is minekünk! A
Magyar nyelvet pedig ne azért beszéljék, mert terjesztjük, hanem mert
fegyver az: élesebb mint a kard; olyan nyílvesszők szavai, amikkel az
időn és a teren is keresztül lehet hatolni – az itt leírtakat csakis
Magyarul lehet igazán érteni.

<span id="anchor-69"></span>Győzd meg a gazdagok utódait
--------------------------------------------------------

Tudással fel sem érhető uradalom,\
Más néven szólva: a háttérhatalom,\
Fejeit levágni bárhogy is akarom,\
Karddal lesújtva fárad el a karom,\
Mégis utat talál hozzá furfangom,\
Nyilam szívébe nyit sebet nagyon,\
És új ellensége: fia-lánya vagyon!

Ha már tiltani fogják a könyvet, ha már tiltani fogják az igaz
embereket, mint barátokat, ha már búrában nevelik fel és tartják az
igazán gazdag ifjakat nehogy ehhez hasonló gondolatokkal találkozzanak,
akkor közel a zárszó: jajj ha egy nyíl is áthalad! Lebecsülöd tán a
váratlan hős poronytokat! Lesz ez csak idő kérdése és belülről
felszakad: Góliát! Most majd Magor íja is elragad! Nem csak a parittya
hozhatja a pusztulásodat.

Aki meggyőzi őket az már győzelmet aratott. A nyelvben is láthatod,
hiába tagadod. Ha esetleg ezt valaha majd úgy olvasod, valódi fiatal
úrként, titokban szorongatod – büszke vagyok rá, de sok a feladatod.

<span id="anchor-70"></span>Légy türelmes – az aranymag örök
------------------------------------------------------------

Az igaz aranymag soha nem vész el,\
Ha földbe vetetted, majd egyszer kikel,\
Ilyen egy napba öltözött és igazi jel,\
Ami örök, ha kell hát örökké telel!

<span id="anchor-71"></span>Lelki és testi önvédelem
----------------------------------------------------

TODO: Főleg a lelkiekről – mert az sokkal fontosabb.

<span id="anchor-72"></span>Hősiességről és bölcsességről
---------------------------------------------------------

<span id="anchor-73"></span>Szeretetről és hűségről
---------------------------------------------------

### <span id="anchor-74"></span>A világunk tagjainak felébresztése

### 


